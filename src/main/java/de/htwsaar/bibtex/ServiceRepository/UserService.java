package de.htwsaar.bibtex.ServiceRepository;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

public class UserService {
	 public static Long getCurrentUserId() {
	        FacesContext fc = FacesContext.getCurrentInstance();
	        ExternalContext externalContext = fc.getExternalContext();
	        if (externalContext.getUserPrincipal() == null) {
	            return null;
	        }
	        return Long.parseLong(externalContext.getUserPrincipal().getName());
	    }
}
