package de.htwsaar.bibtex.ServiceRepository;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.jbibtex.*;

import de.htwsaar.bibtex.ObjectClasses.ArticleEntity;
import de.htwsaar.bibtex.ObjectClasses.BookEntity;
import de.htwsaar.bibtex.ObjectClasses.BookletEntity;
import de.htwsaar.bibtex.ObjectClasses.ConferenceEntity;
import de.htwsaar.bibtex.ObjectClasses.InbookEntity;
import de.htwsaar.bibtex.ObjectClasses.IncollectionEntity;
import de.htwsaar.bibtex.ObjectClasses.InproceedingsEntity;
import de.htwsaar.bibtex.ObjectClasses.ManualEntity;
import de.htwsaar.bibtex.ObjectClasses.MastersthesisEntity;
import de.htwsaar.bibtex.ObjectClasses.MiscEntity;
import de.htwsaar.bibtex.ObjectClasses.PhdthesisEntity;
import de.htwsaar.bibtex.ObjectClasses.ProceedingsEntity;
import de.htwsaar.bibtex.ObjectClasses.TechreportEntity;
import de.htwsaar.bibtex.ObjectClasses.UnpublishedEntity;
import de.htwsaar.bibtex.database.DatabaseEntitys.*;


/**
 * Created by mikegoebel on 25.10.16.
 */
public class FileUploadService {

		private ArticleDaoImpl articledaoImpl = new ArticleDaoImpl();
		private BookDaoImpl bookdaoImpl = new BookDaoImpl();
		private BookletDaoImpl bookletdaoImpl = new BookletDaoImpl();
		private ConferenceDaoImpl conferencedaoImpl = new ConferenceDaoImpl();
		private InbookDaoImpl inbookdaoImpl = new InbookDaoImpl();
		private IncollectionDaoImpl incollectiondaoImpl = new IncollectionDaoImpl();
		private InproceedingsDaoImpl inproceedingsdaoImpl = new InproceedingsDaoImpl();
		private ManualDaoImpl manualdaoImpl = new ManualDaoImpl();
		private MastersthesisDaoImpl masterthesisdaoImpl = new MastersthesisDaoImpl();
		private MiscDaoImpl miscdaoImpl = new MiscDaoImpl();
		private PhdthesisDaoImpl pdhthesisdaoImpl = new PhdthesisDaoImpl();
		private ProceedingsDaoImpl proceedingsdaoImpl = new ProceedingsDaoImpl();
		private TechreportDaoImpl techreportdaoImpl = new TechreportDaoImpl();
		private UnpublishedDaoImpl unpublisheddaoImpl = new UnpublishedDaoImpl();
	    BibTeXDatabase database = new BibTeXDatabase();
	    int length;
	    String tmpFilepath;
	    private long userid;
	    private static final String TYP_ARTICLE = "article";
	    private static final String TYPE_BOOK ="book";
	    private static final String TYPE_BOOKLET = "booklet";
	    private static final String TYPE_CONFERENCE = "conference";
	    private static final String TYPE_INBOOK = "inbook";
	    private static final String TYPE_INCOLLECTION = "incollection";
	    private static final String TYPE_INPROCEEDINGS = "inproceedings";
	    private static final String TYPE_MANUAL = "manual";
	    private static final String TYPE_MASTERSTHESIS = "mastersthesis";
	    private static final String TYPE_MISC = "misc";
	    private static final String TYPE_PHDTHESIS = "phdthesis";
	    private static final String TYPE_PROCEEDINGS ="proceedings";
	    private static final String TYPE_TECHREPORT ="techreport";
	    private static final String TYPE_UNPUBLISHED = "unpublished";
	    
	    public ArrayList<ArticleEntity> articles = new ArrayList<ArticleEntity>();
	    public ArrayList<BookEntity> books = new ArrayList<BookEntity>();
	    public ArrayList<BookletEntity> booklets = new ArrayList<BookletEntity>();
	    public ArrayList<ConferenceEntity> conferences = new ArrayList<ConferenceEntity>();
	    public ArrayList<InbookEntity> inbooks = new ArrayList<InbookEntity>();
	    public ArrayList<IncollectionEntity> incollections = new ArrayList<IncollectionEntity>();
	    public ArrayList<InproceedingsEntity> inproceedings = new ArrayList<InproceedingsEntity>();
	    public ArrayList<ManualEntity> manuals = new ArrayList<ManualEntity>();
	    public ArrayList<MastersthesisEntity> masterthesis = new ArrayList<MastersthesisEntity>();
	    public ArrayList<MiscEntity> miscs = new ArrayList<MiscEntity>();
	    public ArrayList<PhdthesisEntity> phdthesis = new ArrayList<PhdthesisEntity>();
	    public ArrayList<ProceedingsEntity> proceedings = new ArrayList<ProceedingsEntity>();
	    public ArrayList<TechreportEntity> techreport = new ArrayList<TechreportEntity>();
	    public ArrayList<UnpublishedEntity> unpublished = new ArrayList<UnpublishedEntity>();
	   
	    private ArrayList<String> articlekeys = new ArrayList<String>(Arrays.asList("author","title","journal","year","volume","number","pages","month","note"));
	    private ArrayList<String> bookkeys = new ArrayList<String>(Arrays.asList("author","title","publisher","year","volume","series","address","edition","month"));
	    private ArrayList<String> bookletkeys = new ArrayList<String>(Arrays.asList("author","title","howpublished","address","month","year","note"));
	    private ArrayList<String> conferencekeys = new ArrayList<String>(Arrays.asList("author","title","booktitle","year","editor","volume","series","pages","address","month","organization","publisher","note"));
	    private ArrayList<String> inbookkeys = new ArrayList<String>(Arrays.asList("author","title","chapter","pages","publisher","year","volume","series","type","address","edition","month","note"));
	    private ArrayList<String> incollectionkeys = new ArrayList<String>(Arrays.asList("author","title","booktitle","publisher","year","editor","volume","series","type","chapter","pages","address","edition","month","note"));
	    private ArrayList<String> inproceedingskeys = new ArrayList<String>(Arrays.asList("author","title","booktitle","year","editor","volume","series","pages","address","month","organization","publisher","note"));
	    private ArrayList<String> manualkeys = new ArrayList<String>(Arrays.asList("author","title","address","year","organization","edition","month","note"));
	    private ArrayList<String> masterthesiskeys = new ArrayList<String>(Arrays.asList("author","title","school","year","type","address","month","note"));
	    private ArrayList<String> misckeys = new ArrayList<String>(Arrays.asList("author","title","howpublished","year","month","note"));
	    private ArrayList<String> phdthesiskeys = new ArrayList<String>(Arrays.asList("author","title","school","year","type","address","month","note"));
	    private ArrayList<String> proceedingskeys = new ArrayList<String>(Arrays.asList("title","editor","year","volume","series","address","month","note","organization","publisher"));
	    private ArrayList<String> techreportkeys = new ArrayList<String>(Arrays.asList("author","title","institution","year","type","number","address","month","note"));
	    private ArrayList<String> unpublishedkeys = new ArrayList<String>(Arrays.asList("author","title","year","month","note"));

	    public void importBibTex(String path,long userid) throws IOException, ParseException {
	        //FileReader fr = new FileReader(path);
	    	this.userid = userid;
	        File input = new File(path);
	        this.tmpFilepath = controllFileForEscape(input);
	        System.out.print(input.getAbsolutePath());

	        File tmpinput = new File(tmpFilepath);
	         length = 0;
	         database = parseBibTeX(tmpinput);
	         typeInitEntries();
	         addAllList();
	         tmpinput.delete();

	    }

	     public BibTeXDatabase parseBibTeX(File file) throws IOException, ParseException {
	        Reader reader = new FileReader(file);

	        try {
	            BibTeXParser parser = new BibTeXParser(){

	                @Override
	                public void checkStringResolution(Key key, BibTeXString string){

	                    if(string == null){
	                        System.err.println("Unresolved string: \"" + key.getValue() + "\"");
	                    }
	                }

	                @Override
	                public void checkCrossReferenceResolution(Key key, BibTeXEntry entry){

	                    if(entry == null){
	                        System.err.println("Unresolved cross-reference: \"" + key.getValue() + "\"");
	                    }
	                }
	            };

	            return parser.parse(reader);
	        } finally {
	            reader.close();
	        }
	    }
	    static public List<LaTeXObject> parseLaTeX(String string) throws IOException, ParseException {
	        Reader reader = new StringReader(string);

	        try {
	            LaTeXParser parser = new LaTeXParser();

	            return parser.parse(reader);
	        } finally {
	            reader.close();
	        }
	    }

	    static public String printLaTeX(List<LaTeXObject> objects){
	        LaTeXPrinter printer = new LaTeXPrinter();

	        return printer.print(objects);
	    }

	    private String controllFileForEscape(File file) throws IOException {
	       return replaceFileString(file,"\\r","");
	    }
	    public static String replaceFileString(File file,String old, String neu) throws IOException {
	        //String fileName = Settings.getValue("fileDirectory");
	        String tmppathFile = System.getProperty("user.dir")+"/Dokumente";
	        File tempFile = new File(tmppathFile);
	        System.out.print(tempFile.getAbsolutePath()+"\n");
	        FileInputStream fis = new FileInputStream(file);
	        String content = IOUtils.toString(fis, Charset.defaultCharset());
	        content = content.replaceAll(old, neu);
	        FileOutputStream fos = new FileOutputStream(tempFile);
	        IOUtils.write(content, fos, Charset.defaultCharset());
	        fis.close();
	        fos.close();
	        return tmppathFile;
	    }

	    private void typeInitEntries() throws IOException, ParseException {
	        Collection<BibTeXEntry> entries = (this.database.getEntries()).values();
	        for (BibTeXEntry entry : entries) {
	            String type = entry.getType().toString().toLowerCase();
	            if (type.equals(TYP_ARTICLE)) {
	                initArticles(entry);
	            }else if(type.equals(TYPE_BOOK)){
	                initBooks(entry);
	            }else if(type.equals(TYPE_BOOKLET)){
	            	initBooklet(entry);
	            }else if(type.equals(TYPE_CONFERENCE)){
	            	initConference(entry);
	            }else if(type.equals(TYPE_INBOOK)){
	            	initInbook(entry);
	            }else if(type.equals(TYPE_INCOLLECTION)){
	            	initIncollection(entry);
	            }else if(type.equals(TYPE_INPROCEEDINGS)){
	            	initInproceedings(entry);
	            }else if(type.equals(TYPE_MANUAL)){
	            	initManual(entry);
	            }else if(type.equals(TYPE_MASTERSTHESIS)){
	            	initMasterthesis(entry);
	            }else if(type.equals(TYPE_MISC)){
	            	initMisc(entry);
	            }else if(type.equals(TYPE_PHDTHESIS)){
	            	initPhdthesis(entry);
	            }else if(type.equals(TYPE_PROCEEDINGS)){
	            	initProceedings(entry);
	            }else if(type.equals(TYPE_TECHREPORT)){
	            	initTechreport(entry);
	            }else if(type.equals(TYPE_UNPUBLISHED)){
	            	initUnpublished(entry);
	            }
	        }
	    }

	    private void initArticles(BibTeXEntry entry) throws IOException, ParseException {
	        ArticleEntity a = new ArticleEntity();
	        for (int i = 0; i < articlekeys.size(); i++) {
	            Value value = entry.getField(new Key(articlekeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        a.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        a.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        a.setJournal(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        a.setYear(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        a.setVolume(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        a.setNumber(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        a.setPages(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        a.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 8:
	                        a.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        a.setProfid(userid);
	        articles.add(a);
	    }

	    private void initBooks(BibTeXEntry entry) throws IOException, ParseException {
	       BookEntity b = new BookEntity();
	        for (int i = 0; i < bookkeys.size(); i++) {
	            Value value = entry.getField(new Key(bookkeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                       b.setAuthor(plainText(value.toUserString()));
	                       break;
	                    case 1:
	                       b.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                       b.setPublisher(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        b.setYear(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        b.setVolume(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        b.setSeries(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        b.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        b.setEdition(plainText(value.toUserString()));
	                        break;
	                    case 8:
	                       b.setMonth(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        b.setProfid(userid);
	        books.add(b);
	    }
	    private void initBooklet(BibTeXEntry entry) throws IOException, ParseException {
	        BookletEntity b = new BookletEntity();
	        for (int i = 0; i < bookletkeys.size(); i++) {
	            Value value = entry.getField(new Key(bookletkeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        b.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        b.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        b.setHowpublished(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        b.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        b.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        b.setYear(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        b.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        b.setProfid(userid);
	        booklets.add(b);
	    }
	    private void initConference(BibTeXEntry entry) throws IOException, ParseException {
	        ConferenceEntity c = new ConferenceEntity();
	        for (int i = 0; i < conferencekeys.size(); i++) {
	            Value value = entry.getField(new Key(conferencekeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                       c.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        c.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        c.setBooktitle(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        c.setYear(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        c.setEditor(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        c.setVolume(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        c.setSeries(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        c.setPages(plainText(value.toUserString()));
	                        break;
	                    case 8:
	                        c.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 9:
	                        c.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 10:
	                        c.setOrganization(plainText(value.toUserString()));
	                        break;
	                    case 11:
	                        c.setPublisher(plainText(value.toUserString()));
	                        break;
	                    case 12:
	                        c.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        c.setProfid(userid);
	        conferences.add(c);
	    }
	    private void initInbook(BibTeXEntry entry) throws IOException, ParseException {
	        InbookEntity in = new InbookEntity();
	        for (int i = 0; i < inbookkeys.size(); i++) {
	            Value value = entry.getField(new Key(inbookkeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        in.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        in.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        in.setChapter(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        in.setPages(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        in.setPublisher(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        in.setYear(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        in.setVolume(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        in.setSeries(plainText(value.toUserString()));
	                        break;
	                    case 8:
	                        in.setType(plainText(value.toUserString()));
	                        break;
	                    case 9:
	                        in.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 10:
	                        in.setEdition(plainText(value.toUserString()));
	                        break;
	                    case 11:
	                        in.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 12:
	                        in.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        in.setProfid(userid);
	        inbooks.add(in);
	    }
	    private void initIncollection(BibTeXEntry entry) throws IOException, ParseException {
	        IncollectionEntity in = new IncollectionEntity();
	        for (int i = 0; i < incollectionkeys.size(); i++) {
	            Value value = entry.getField(new Key(incollectionkeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        in.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        in.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        in.setBooktitle(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        in.setPublisher(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        in.setYear(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        in.setEditor(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        in.setVolume(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        in.setSeries(plainText(value.toUserString()));
	                        break;
	                    case 8:
	                        in.setType(plainText(value.toUserString()));
	                        break;
	                    case 9:
	                        in.setChapter(plainText(value.toUserString()));
	                        break;
	                    case 10:
	                        in.setPages(plainText(value.toUserString()));
	                        break;
	                    case 11:
	                        in.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 12:
	                        in.setEdition(plainText(value.toUserString()));
	                        break;
	                    case 13:
	                        in.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 14:
	                        in.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        in.setProfid(userid);
	        incollections.add(in);
	    }
	    private void initInproceedings(BibTeXEntry entry) throws IOException, ParseException {
	        InproceedingsEntity in = new InproceedingsEntity();
	        for (int i = 0; i < inproceedingskeys.size(); i++) {
	            Value value = entry.getField(new Key(inproceedingskeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                case 0:
	                        in.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        in.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        in.setBooktitle(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        in.setYear(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        in.setEditor(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        in.setVolume(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        in.setSeries(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        in.setPages(plainText(value.toUserString()));
	                        break;
	                    case 8:
	                        in.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 9:
	                        in.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 10:
	                        in.setOrganization(plainText(value.toUserString()));
	                        break;
	                    case 11:
	                        in.setPublisher(plainText(value.toUserString()));
	                        break;
	                    case 12:
	                        in.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        in.setProfid(userid);
	        inproceedings.add(in);
	    }
	    private void initManual(BibTeXEntry entry) throws IOException, ParseException {
	        ManualEntity m = new ManualEntity();
	        for (int i = 0; i < manualkeys.size(); i++) {
	            Value value = entry.getField(new Key(manualkeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        m.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        m.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        m.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        m.setYear(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        m.setOrganization(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        m.setEdition(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        m.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        m.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        m.setProfid(userid);
	        manuals.add(m);
	    }
	    private void initMasterthesis(BibTeXEntry entry) throws IOException, ParseException {
	        MastersthesisEntity m = new MastersthesisEntity();
	        for (int i = 0; i < masterthesiskeys.size(); i++) {
	            Value value = entry.getField(new Key(masterthesiskeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        m.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        m.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        m.setSchool(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        m.setYear(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        m.setType(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        m.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        m.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        m.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        m.setProfid(userid);
	        masterthesis.add(m);
	    }
	    private void initMisc(BibTeXEntry entry) throws IOException, ParseException {
	        MiscEntity m = new MiscEntity();
	        for (int i = 0; i < misckeys.size(); i++) {
	            Value value = entry.getField(new Key(misckeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        m.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        m.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        m.setHowpublished(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        m.setYear(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        m.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        m.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        m.setProfid(userid);
	        miscs.add(m);
	    }
	    private void initPhdthesis(BibTeXEntry entry) throws IOException, ParseException {
	        PhdthesisEntity p = new PhdthesisEntity();
	        for (int i = 0; i < phdthesiskeys.size(); i++) {
	            Value value = entry.getField(new Key(phdthesiskeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        p.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        p.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        p.setSchool(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        p.setYear(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        p.setType(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        p.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        p.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        p.setNote(plainText(value.toUserString()));
	                        break;             
	                }
	            }
	        }
	        p.setProfid(userid);
	        phdthesis.add(p);
	    }
	    private void initProceedings(BibTeXEntry entry) throws IOException, ParseException {
	        ProceedingsEntity p = new ProceedingsEntity();
	        for (int i = 0; i < proceedingskeys.size(); i++) {
	            Value value = entry.getField(new Key(proceedingskeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                    	p.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        p.setEditor(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                    	p.setYear(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                    	p.setVolume(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        p.setSeries(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        p.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        p.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                    	p.setNote(plainText(value.toUserString()));
	                        break;
	                    case 8:
	                        p.setOrganization(plainText(value.toUserString()));
	                        break;
	                    case 9:
	                        p.setPublisher(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        p.setProfid(userid);
	        proceedings.add(p);
	    }
	    private void initTechreport(BibTeXEntry entry) throws IOException, ParseException {
	        TechreportEntity t = new TechreportEntity();
	        for (int i = 0; i < techreportkeys.size(); i++) {
	            Value value = entry.getField(new Key(techreportkeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        t.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        t.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                        t.setInstitution(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                        t.setYear(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                        t.setType(plainText(value.toUserString()));
	                        break;
	                    case 5:
	                        t.setNumber(plainText(value.toUserString()));
	                        break;
	                    case 6:
	                        t.setAddress(plainText(value.toUserString()));
	                        break;
	                    case 7:
	                        t.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 8:
	                        t.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        t.setProfid(userid);
	        techreport.add(t);
	    }
	    private void initUnpublished(BibTeXEntry entry) throws IOException, ParseException {
	        UnpublishedEntity u = new UnpublishedEntity();
	        for (int i = 0; i < unpublishedkeys.size(); i++) {
	            Value value = entry.getField(new Key(unpublishedkeys.get(i)));
	            if (value == null) {
	                continue;
	            }else{
	                switch (i) {
	                    case 0:
	                        u.setAuthor(plainText(value.toUserString()));
	                        break;
	                    case 1:
	                        u.setTitle(plainText(value.toUserString()));
	                        break;
	                    case 2:
	                    	u.setYear(plainText(value.toUserString()));
	                        break;
	                    case 3:
	                    	u.setMonth(plainText(value.toUserString()));
	                        break;
	                    case 4:
	                    	u.setNote(plainText(value.toUserString()));
	                        break;
	                }
	            }
	        }
	        u.setProfid(userid);
	        unpublished.add(u);
	    }
	   

	    private String plainText(String unformatted) throws IOException, ParseException {
	        try {
	            if (unformatted.indexOf('\\') > -1 || unformatted.indexOf('{') > -1) {
	                List<LaTeXObject> laTeXObjects = parseLaTeX(unformatted);
	                return printLaTeX(laTeXObjects);
	            } else {
	                return unformatted;
	            }
	        }catch(ParseException e){
	            return unformatted;
	        }
	    }
	    
	    private void addListArticle(){
	    	articles.forEach((a)->{
	    	ArticleEntity article = new ArticleEntity();
	    	article = articledaoImpl.existsArticle(a);
	    	if(article == null){
	    		articledaoImpl.insert(a);
	    	}else{
	    		a.setId(article.getId());
	    		articledaoImpl.update(a);
	    	}});
	    }
	    private void addListBook(){
	    	books.forEach((b)->{
		    	BookEntity book = new BookEntity();
		    	book = bookdaoImpl.existsbook(b);
		    	if(book == null){
		    		bookdaoImpl.insert(b);
		    	}else{
		    		b.setId(book.getId());
		    		bookdaoImpl.update(b);
		    	}});
	    }
	    private void addListBooklet(){
	    	booklets.forEach((b)->{
		    	BookletEntity booklet = new BookletEntity();
		    	booklet = bookletdaoImpl.existsbooklet(b);
		    	if(booklet == null){
		    		bookletdaoImpl.insert(b);
		    	}else{
		    		b.setId(booklet.getId());
		    		bookletdaoImpl.update(b);
		    	}});
	    }
	    private void addListConference(){
	    	conferences.forEach((c)->{
		    	ConferenceEntity conference = new ConferenceEntity();
		    	conference = conferencedaoImpl.existsconference(c);
		    	if(conference == null){
		    		conferencedaoImpl.insert(c);
		    	}else{
		    		c.setId(conference.getId());
		    		conferencedaoImpl.update(c);
		    	}});
	    }
	    private void addListInbook(){
	    	inbooks.forEach((in)->{
		    	InbookEntity inbook = new InbookEntity();
		    	inbook = inbookdaoImpl.existsinbook(in);
		    	if(inbook == null){
		    		inbookdaoImpl.insert(in);
		    	}else{
		    		in.setId(inbook.getId());
		    		inbookdaoImpl.update(in);
		    	}});
	    }
	    private void addListIncollection(){
	    	incollections.forEach((in)->{
		    	IncollectionEntity incollection = new IncollectionEntity();
		    	incollection = incollectiondaoImpl.existsincollection(in);
		    	if(incollection == null){
		    		incollectiondaoImpl.insert(in);
		    	}else{
		    		in.setId(incollection.getId());
		    		incollectiondaoImpl.update(in);
		    	}});
	    }
	    private void addListManual(){
	    	manuals.forEach((m)->{
		    	ManualEntity manuals = new ManualEntity();
		    	manuals = manualdaoImpl.existsmanual(m);
		    	if(manuals == null){
		    		manualdaoImpl.insert(m);
		    	}else{
		    		m.setId(manuals.getId());
		    		manualdaoImpl.update(m);
		    	}});
	    }
	    private void addListMasterthesis(){
	    	masterthesis.forEach((m)->{
		    	MastersthesisEntity master = new MastersthesisEntity();
		    	master = masterthesisdaoImpl.existsmasterthesis(m);
		    	if(master == null){
		    		masterthesisdaoImpl.insert(m);
		    	}else{
		    		m.setId(master.getId());
		    		masterthesisdaoImpl.update(m);
		    	}});
	    }
	    private void addListMisc(){
	    	miscs.forEach((m)->{
		    	MiscEntity misc = new MiscEntity();
		    	misc = miscdaoImpl.existsmisc(m);
		    	if(misc == null){
		    		miscdaoImpl.insert(m);
		    	}else{
		    		m.setId(misc.getId());
		    		miscdaoImpl.update(m);
		    	}});
	    }
	    private void addListPdhthesis(){
	    	phdthesis.forEach((p)->{
		    	PhdthesisEntity phd = new PhdthesisEntity();
		    	phd = pdhthesisdaoImpl.existsphdthesis(p);
		    	if(phd == null){
		    		pdhthesisdaoImpl.insert(p);
		    	}else{
		    		p.setId(phd.getId());
		    		pdhthesisdaoImpl.update(p);
		    	}});
	    }
	    private void addListProceedings(){
	    	proceedings.forEach((p)->{
		    	ProceedingsEntity pro = new ProceedingsEntity();
		    	pro = proceedingsdaoImpl.existsproceedings(p);
		    	if(pro == null){
		    		proceedingsdaoImpl.insert(p);
		    	}else{
		    		p.setId(pro.getId());
		    		proceedingsdaoImpl.update(p);
		    	}});
	    }
	    private void addListTechreport(){
	    	techreport.forEach((t)->{
		    	TechreportEntity tech = new TechreportEntity();
		    	tech = techreportdaoImpl.existstechreport(t);
		    	if(tech == null){
		    		techreportdaoImpl.insert(t);
		    	}else{
		    		t.setId(tech.getId());
		    		techreportdaoImpl.update(t);
		    	}});
	    }
	    private void addListUnpublished(){
	    	unpublished.forEach((u)->{
		    	UnpublishedEntity un = new UnpublishedEntity();
		    	un = unpublisheddaoImpl.existsunpublished(u);
		    	if(un == null){
		    		unpublisheddaoImpl.insert(u);
		    	}else{
		    		u.setId(un.getId());
		    		unpublisheddaoImpl.update(u);
		    	}});
	    }
	    private void addListInproceedings(){
	    	inproceedings.forEach((in)->{
		    	InproceedingsEntity inpro = new InproceedingsEntity();
		    	inpro = inproceedingsdaoImpl.existsinproceedings(in);
		    	if(inpro == null){
		    		inproceedingsdaoImpl.insert(in);
		    	}else{
		    		in.setId(inpro.getId());
		    		inproceedingsdaoImpl.update(in);
		    	}});
	    }
	    
	    private void addAllList(){
		    addListArticle();
		    addListBook();
		    addListBooklet();
		    addListConference();
		    addListInbook();
		    addListIncollection();
		    addListManual();
		    addListMasterthesis();
		    addListMisc();
		    addListPdhthesis();
		    addListProceedings();
		    addListTechreport();
		    addListUnpublished();
		    addListInproceedings();
	    }
}



