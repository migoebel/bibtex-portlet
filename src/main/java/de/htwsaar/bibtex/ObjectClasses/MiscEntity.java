package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Misc;

import javax.persistence.*;

/**
 * Created by mikegoebel on 16.01.17.
 */
@Entity
@Table(name = "misc")
public class MiscEntity implements Misc{

    private int id;
    private String author;
    private String title;
    private String howpublished;
    private String month;
    private String year;
    private String note;

    private long profid;
    
    public MiscEntity(){}

    public MiscEntity(int id, String author, String title, String howpublished, String month, String year, String note, long profid) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.howpublished = howpublished;
        this.month = month;
        this.year = year;
        this.note = note;
        this.profid = profid;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHowpublished() {
        return howpublished;
    }

    public void setHowpublished(String howpublished) {
        this.howpublished = howpublished;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

	public long getProfid() {
		return profid;
	}

	public void setProfid(long profid) {
		this.profid = profid;
	}

 
}
