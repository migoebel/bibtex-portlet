package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Techreport;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;


/**
 * Created by mikegoebel on 16.01.17.
 */
@Entity
@Table(name = "unpublished")
public class TechreportEntity implements Techreport{

    @NotEmpty
    private int id;
    @NotEmpty
    private String author;
    @NotEmpty
    private String title;
    @NotEmpty
    private String institution;
    @NotEmpty
    private String year;
    //optional fields

    private String type;
    private String note;
    private String number;
    private String address;
    private String month;

    private long profid;

    public TechreportEntity(){}

    public TechreportEntity(int id, String author, String title, String institution, String year, String type, String note, String number, String address, String month, long profid) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.institution = institution;
        this.year = year;
        this.type = type;
        this.note = note;
        this.number = number;
        this.address = address;
        this.month = month;
        this.profid = profid;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

	public long getProfid() {
		return profid;
	}

	public void setProfid(long profid) {
		this.profid = profid;
	}

  
}
