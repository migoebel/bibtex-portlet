package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Manual;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mikegoebel on 16.01.17.
 */
@Entity
@Table(name = "manual")
public class ManualEntity implements Manual{

    @NotEmpty
    private int id;
    @NotEmpty
    private String address;
    @NotEmpty
    private String title;
    @NotEmpty
    private String year;
    //optional Fields

    private String author;
    private String organization;
    private String edition;
    private String month;
    private String note;

    private long profid;

    public ManualEntity(){

    }

    public ManualEntity(int id, String address, String title, String year, String author, String organization, String edition, String month, String note, long profid) {
        this.id = id;
        this.address = address;
        this.title = title;
        this.year = year;
        this.author = author;
        this.organization = organization;
        this.edition = edition;
        this.month = month;
        this.note = note;
        this.profid = profid;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

	public long getProfid() {
		return profid;
	}

	public void setProfid(long profid) {
		this.profid = profid;
	}

    
}
