package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Unpublished;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mikegoebel on 16.01.17.
 */
@Entity
@Table(name = "unpublished")
public class UnpublishedEntity implements Unpublished{

    @NotEmpty
    private int id;
    @NotEmpty
    private String author;
    @NotEmpty
    private String title;
    @NotEmpty
    private String note;
    //optional fields
    private String month;
    private String year;

    private long profid;
    
    public UnpublishedEntity() {
    }


    public UnpublishedEntity(int id, String author, String title, String note, String month, String year, long profid) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.note = note;
        this.month = month;
        this.year = year;
        this.profid = profid;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }


	public long getProfid() {
		return profid;
	}


	public void setProfid(long profid) {
		this.profid = profid;
	}


}