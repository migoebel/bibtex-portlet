package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Book;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mikegoebel on 19.12.16.
 */

@Entity
@Table(name = "book")
public class BookEntity implements Book{

        private String id;
        //oder Editor
        @NotEmpty
        private String author;
        @NotEmpty
        private String title;
        @NotEmpty
        private String publisher;
        @NotEmpty
        private String year;
        //Oder Number
        private String volume;
        private String series;
        private String address;
        private String edition;
        private String month;
        private String note;
        private String isbn;

        private long profid;

        public BookEntity(){

        }


    public BookEntity(String id, String author, String title, String publisher, String year, String volume, String series, String address, String edition, String month, String note, String isbn, long profid) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.publisher = publisher;
        this.year = year;
        this.volume = volume;
        this.series = series;
        this.address = address;
        this.edition = edition;
        this.month = month;
        this.note = note;
        this.isbn = isbn;
        this.profid = profid;
    }

    	@Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPublisher() {
            return publisher;
        }

        public void setPublisher(String publisher) {
            this.publisher = publisher;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getVolume() {
            return volume;
        }

        public void setVolume(String volume) {
            this.volume = volume;
        }

        public String getSeries() {
            return series;
        }

        public void setSeries(String series) {
            this.series = series;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEdition() {
            return edition;
        }

        public void setEdition(String edition) {
            this.edition = edition;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getIsbn() {
            return isbn;
        }

        public void setIsbn(String isbn) {
            this.isbn = isbn;
        }


		public long getProfid() {
			return profid;
		}


		public void setProfid(long profid) {
			this.profid = profid;
		}
        
   
    }

