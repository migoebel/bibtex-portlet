package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Inbook;

import javax.persistence.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mikegoebel on 11.01.17.
 */
@Entity
@Table(name = "inbook")
public class InbookEntity implements Inbook{
    private int id;

    @NotEmpty
    private String author;
    @NotEmpty
    private String title;
    @NotEmpty
    private String chapter;
    @NotEmpty
    private String pages;
    @NotEmpty
    private String publisher;
    @NotEmpty
    private String year;

    //optional Fields

    private String volume;
    private String series;
    private String type;
    private String address;
    private String edition;
    private String month;
    private String note;

    private long profid;

    public InbookEntity(){

    }

    public InbookEntity(int id, String author, String title, String chapter, String pages, String publisher, String year, String volume, String series, String type, String address, String edition, String month, String note, long profid) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.chapter = chapter;
        this.pages = pages;
        this.publisher = publisher;
        this.year = year;
        this.volume = volume;
        this.series = series;
        this.type = type;
        this.address = address;
        this.edition = edition;
        this.month = month;
        this.note = note;
        this.profid = profid;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

	public long getProfid() {
		return profid;
	}

	public void setProfid(long profid) {
		this.profid = profid;
	}

    
}
