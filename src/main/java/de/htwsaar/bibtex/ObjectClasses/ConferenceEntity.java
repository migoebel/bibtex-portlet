package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Conference;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mikegoebel on 11.01.17.
 */
@Entity
@Table(name = "conference")
public class ConferenceEntity implements Conference{

    private int id;

    @NotEmpty
    private String author;
    @NotEmpty
    private String title;
    @NotEmpty
    private String booktitle;
    @NotEmpty
    private String year;
    //Optional Fields

    private String editor;
    private String volume;
    private String series;
    private String pages;
    private String address;
    private String month;
    private String organization;
    private String publisher;
    private String note;

    private long profid;

    public ConferenceEntity(){

    }


    public ConferenceEntity(int id, String author, String title, String booktitle, String year, String editor, String volume, String series, String pages, String address, String month, String organization, String publisher, String note, long profid) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.booktitle = booktitle;
        this.year = year;
        this.editor = editor;
        this.volume = volume;
        this.series = series;
        this.pages = pages;
        this.address = address;
        this.month = month;
        this.organization = organization;
        this.publisher = publisher;
        this.note = note;
        this.profid = profid;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBooktitle() {
        return booktitle;
    }

    public void setBooktitle(String booktitle) {
        this.booktitle = booktitle;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


	public long getProfid() {
		return profid;
	}


	public void setProfid(long profid) {
		this.profid = profid;
	}

    
 
}
