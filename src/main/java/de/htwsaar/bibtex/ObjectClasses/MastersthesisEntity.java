package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Masterthesis;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mikegoebel on 16.01.17.
 */
@Entity
@Table(name = "mastersthesis")
public class MastersthesisEntity implements Masterthesis{

    @NotEmpty
    private int id;
    @NotEmpty
    private String author;
    @NotEmpty
    private String title;
    @NotEmpty
    private String school;
    @NotEmpty
    private String year;
    //optional fields

    private String type;
    private String address;
    private String month;
    private String note;

    private long profid;

    public MastersthesisEntity(){

    }

    public MastersthesisEntity(int id, String author, String title, String school, String year, String type, String address, String month, String note, long profid) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.school = school;
        this.year = year;
        this.type = type;
        this.address = address;
        this.month = month;
        this.note = note;
        this.profid = profid;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

	public long getProfid() {
		return profid;
	}

	public void setProfid(long profid) {
		this.profid = profid;
	}

  
}
