package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Booklet;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mikegoebel on 11.01.17.
 */
@Entity
@Table(name = "booklet")
public class BookletEntity implements Booklet{

    private int id;
    @NotEmpty
    private String title;
    //Optional Fields
    private String author;
    private String howpublished;
    private String address;
    private String month;
    private String year;
    private String note;

    private long profid;
    
    public BookletEntity(){

    }

    public BookletEntity(int id, String title, String author, String howpublished, String address, String month, String year, String note, long profid) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.howpublished = howpublished;
        this.address = address;
        this.month = month;
        this.year = year;
        this.note = note;
        this.profid = profid;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getHowpublished() {
        return howpublished;
    }

    public void setHowpublished(String howpublished) {
        this.howpublished = howpublished;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

	public long getProfid() {
		return profid;
	}

	public void setProfid(long profid) {
		this.profid = profid;
	}

    
}
