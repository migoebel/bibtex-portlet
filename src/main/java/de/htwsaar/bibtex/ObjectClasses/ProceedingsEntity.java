package de.htwsaar.bibtex.ObjectClasses;

import de.htwsaar.bibtex.InterfaceObjects.Proceedings;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by mikegoebel on 16.01.17.
 */
@Entity
@Table(name = "proceedings")
public class ProceedingsEntity implements Proceedings{

    @NotEmpty
    private int id;
    @NotEmpty
    private String title;
    @NotEmpty
    private String year;
    //optional fields

    private String editor;
    private String volume; //oder number,
    private String series;
    private String address;
    private String month;
    private String organization;
    private String publisher;
    private String note;

    private long profid;

    public ProceedingsEntity(){

    }

    public ProceedingsEntity(int id, String title, String year, String editor, String volume, String series, String address, String month, String organization, String publisher, String note, long profid) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.editor = editor;
        this.volume = volume;
        this.series = series;
        this.address = address;
        this.month = month;
        this.organization = organization;
        this.publisher = publisher;
        this.note = note;
        this.profid = profid;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

	public long getProfid() {
		return profid;
	}

	public void setProfid(long profid) {
		this.profid = profid;
	}

   
}
