package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.ObjectClasses.BookletEntity;
import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.InterfaceObjects.Booklet;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 11.01.17.
 */
public class BookletDaoImpl {
    //Delete bookletObject in Datasource
    public BookletEntity delete(BookletEntity booklet) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(booklet);
        s.getTransaction().commit();
        s.close();
        return booklet;
    }

    public BookletEntity update(BookletEntity booklet) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(booklet);
        s.getTransaction().commit();
        s.close();
        return booklet;
    }

    public BookletEntity insert(BookletEntity booklet) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(booklet);
        s.getTransaction().commit();
        s.close();
        return booklet;
    }

    public List<BookletEntity> getAllbooklet() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM BookletEntity a";
            Query query = s.createQuery(queryString);
           List<BookletEntity> bookletList = query.list();
            s.getTransaction().commit();
            
            return bookletList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public BookletEntity existsbooklet(Booklet a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from BookletEntity where title = :title AND profid = :profid");
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        BookletEntity booklet = (BookletEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(booklet != null) {
            System.out.println(booklet.getId());
            return booklet;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
