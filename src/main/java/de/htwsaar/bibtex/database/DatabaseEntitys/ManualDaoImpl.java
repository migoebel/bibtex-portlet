package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.ObjectClasses.ManualEntity;
import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.InterfaceObjects.Manual;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 16.01.17.
 */
public class ManualDaoImpl {
	
	  //Delete manualObject in Datasource
    public ManualEntity delete(ManualEntity manual) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(manual);
        s.getTransaction().commit();
        s.close();
        return manual;
    }

    public ManualEntity update(ManualEntity manual) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(manual);
        s.getTransaction().commit();
        s.close();
        return manual;
    }

    public ManualEntity insert(ManualEntity manual) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(manual);
        s.getTransaction().commit();
        s.close();
        return manual;
    }

    public List<ManualEntity> getAllmanual() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM ManualEntity a";
            Query query = s.createQuery(queryString);
           List<ManualEntity> manualList = query.list();
            s.getTransaction().commit();
            
            return manualList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public ManualEntity existsmanual(Manual a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from ManualEntity where address = :adress AND title = :title AND profid = :profid AND year = :year");
        query.setParameter("address",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        ManualEntity manual = (ManualEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(manual != null) {
            System.out.println(manual.getId());
            return manual;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
