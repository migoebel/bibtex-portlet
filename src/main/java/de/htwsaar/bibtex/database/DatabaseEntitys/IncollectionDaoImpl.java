package de.htwsaar.bibtex.database.DatabaseEntitys;


import de.htwsaar.bibtex.ObjectClasses.IncollectionEntity;
import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.InterfaceObjects.Incollection;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 11.01.17.
 */
public class IncollectionDaoImpl {
	  //Delete incollectionObject in Datasource
    public IncollectionEntity delete(IncollectionEntity incollection) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(incollection);
        s.getTransaction().commit();
        s.close();
        return incollection;
    }

    public IncollectionEntity update(IncollectionEntity incollection) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(incollection);
        s.getTransaction().commit();
        s.close();
        return incollection;
    }

    public IncollectionEntity insert(IncollectionEntity incollection) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(incollection);
        s.getTransaction().commit();
        s.close();
        return incollection;
    }

    public List<IncollectionEntity> getAllincollection() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM IncollectionEntity a";
            Query query = s.createQuery(queryString);
           List<IncollectionEntity> incollectionList = query.list();
            s.getTransaction().commit();
            
            return incollectionList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public IncollectionEntity existsincollection(Incollection a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from IncollectionEntity where author = :author AND title = :title AND profid = :profid AND year = :year AND publisher = :publisher AND booktitle = :booktitle");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        query.setParameter("publisher",a.getPublisher());
        query.setParameter("booktitle",a.getBooktitle());
        IncollectionEntity incollection = (IncollectionEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(incollection != null) {
            System.out.println(incollection.getId());
            return incollection;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
