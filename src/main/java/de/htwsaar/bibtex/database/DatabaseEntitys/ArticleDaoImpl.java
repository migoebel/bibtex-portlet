package de.htwsaar.bibtex.database.DatabaseEntitys;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import de.htwsaar.bibtex.InterfaceObjects.Article;
import de.htwsaar.bibtex.ObjectClasses.ArticleEntity;
import de.htwsaar.bibtex.database.DBUtil;

/**
 * Created by mikegoebel on 19.12.16.
 */

public class ArticleDaoImpl {

    //Delete ArticleObject in Datasource
    public ArticleEntity delete(ArticleEntity article) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(article);
        s.getTransaction().commit();
        s.close();
        return article;
    }

    public ArticleEntity update(ArticleEntity article) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(article);
        s.getTransaction().commit();
        s.close();
        return article;
    }

    public ArticleEntity insert(ArticleEntity article) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(article);
        s.getTransaction().commit();
        s.close();
        return article;
    }

    public List<ArticleEntity> getAllArticle() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM ArticleEntity a";
            Query query = s.createQuery(queryString);
           List<ArticleEntity> articleList = query.list();
            s.getTransaction().commit();
            
            return articleList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public ArticleEntity existsArticle(Article a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from ArticleEntity where author = :author AND title = :title AND profid = :profid AND year = :year");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        ArticleEntity article = (ArticleEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(article != null) {
            System.out.println(article.getId());
            return article;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
