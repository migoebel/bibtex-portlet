package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.InterfaceObjects.Inbook;
import de.htwsaar.bibtex.ObjectClasses.InbookEntity;
import de.htwsaar.bibtex.database.DBUtil;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 11.01.17.
 */
public class InbookDaoImpl {
	  //Delete inbookObject in Datasource
    public InbookEntity delete(InbookEntity inbook) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(inbook);
        s.getTransaction().commit();
        s.close();
        return inbook;
    }

    public InbookEntity update(InbookEntity inbook) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(inbook);
        s.getTransaction().commit();
        s.close();
        return inbook;
    }

    public InbookEntity insert(InbookEntity inbook) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(inbook);
        s.getTransaction().commit();
        s.close();
        return inbook;
    }

    public List<InbookEntity> getAllinbook() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM InbookEntity a";
            Query query = s.createQuery(queryString);
           List<InbookEntity> inbookList = query.list();
            s.getTransaction().commit();
            
            return inbookList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public InbookEntity existsinbook(Inbook a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from InbookEntity where author = :author AND title = :title AND profid = :profid AND year = :year AND chapter = :chapter AND pages = :pages AND publisher = :publisher");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("chapter",a.getChapter());
        query.setParameter("year",a.getYear());
        query.setParameter("pages",a.getPages());
        query.setParameter("publisher",a.getPublisher());
        InbookEntity inbook = (InbookEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(inbook != null) {
            System.out.println(inbook.getId());
            return inbook;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
