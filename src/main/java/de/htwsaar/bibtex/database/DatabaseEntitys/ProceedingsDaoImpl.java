package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.ObjectClasses.ProceedingsEntity;
import de.htwsaar.bibtex.InterfaceObjects.Proceedings;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 16.01.17.
 */
public class ProceedingsDaoImpl {
	  //Delete proceedingsObject in Datasource
    public ProceedingsEntity delete(ProceedingsEntity proceedings) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(proceedings);
        s.getTransaction().commit();
        s.close();
        return proceedings;
    }

    public ProceedingsEntity update(ProceedingsEntity proceedings) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(proceedings);
        s.getTransaction().commit();
        s.close();
        return proceedings;
    }

    public ProceedingsEntity insert(ProceedingsEntity proceedings) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(proceedings);
        s.getTransaction().commit();
        s.close();
        return proceedings;
    }

    public List<ProceedingsEntity> getAllproceedings() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM ProceedingsEntity a";
            Query query = s.createQuery(queryString);
           List<ProceedingsEntity> proceedingsList = query.list();
            s.getTransaction().commit();
            
            return proceedingsList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public ProceedingsEntity existsproceedings(Proceedings a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from ProceedingsEntity where title = :title AND profid = :profid AND year = :year");
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        ProceedingsEntity proceedings = (ProceedingsEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(proceedings != null) {
            System.out.println(proceedings.getId());
            return proceedings;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
