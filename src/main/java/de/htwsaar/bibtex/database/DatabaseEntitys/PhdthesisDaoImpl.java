package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.ObjectClasses.PhdthesisEntity;
import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.InterfaceObjects.Phdthesis;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 16.01.17.
 */
public class PhdthesisDaoImpl {
	  //Delete phdthesisObject in Datasource
    public PhdthesisEntity delete(PhdthesisEntity phdthesis) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(phdthesis);
        s.getTransaction().commit();
        s.close();
        return phdthesis;
    }

    public PhdthesisEntity update(PhdthesisEntity phdthesis) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(phdthesis);
        s.getTransaction().commit();
        s.close();
        return phdthesis;
    }

    public PhdthesisEntity insert(PhdthesisEntity phdthesis) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(phdthesis);
        s.getTransaction().commit();
        s.close();
        return phdthesis;
    }

    public List<PhdthesisEntity> getAllphdthesis() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM PhdthesisEntity a";
            Query query = s.createQuery(queryString);
           List<PhdthesisEntity> phdthesisList = query.list();
            s.getTransaction().commit();
            
            return phdthesisList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public PhdthesisEntity existsphdthesis(Phdthesis a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from PhdthesisEntity where author = :author AND title = :title AND profid = :profid AND year = :year AND school = :school");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        query.setParameter("school", a.getSchool());
        PhdthesisEntity phdthesis = (PhdthesisEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(phdthesis != null) {
            System.out.println(phdthesis.getId());
            return phdthesis;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
