package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.ObjectClasses.MiscEntity;
import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.InterfaceObjects.Misc;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 16.01.17.
 */
public class MiscDaoImpl {
	  //Delete miscObject in Datasource
    public MiscEntity delete(MiscEntity misc) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(misc);
        s.getTransaction().commit();
        s.close();
        return misc;
    }

    public MiscEntity update(MiscEntity misc) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(misc);
        s.getTransaction().commit();
        s.close();
        return misc;
    }

    public MiscEntity insert(MiscEntity misc) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(misc);
        s.getTransaction().commit();
        s.close();
        return misc;
    }

    public List<MiscEntity> getAllmisc() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM MiscEntity a";
            Query query = s.createQuery(queryString);
           List<MiscEntity> miscList = query.list();
            s.getTransaction().commit();
            
            return miscList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public MiscEntity existsmisc(Misc a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from MiscEntity where author = :author AND title = :title AND profid = :profid AND year = :year");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        MiscEntity misc = (MiscEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(misc != null) {
            System.out.println(misc.getId());
            return misc;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
