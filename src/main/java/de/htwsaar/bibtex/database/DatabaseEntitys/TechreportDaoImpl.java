package de.htwsaar.bibtex.database.DatabaseEntitys;


import de.htwsaar.bibtex.ObjectClasses.TechreportEntity;
import de.htwsaar.bibtex.InterfaceObjects.Techreport;
import de.htwsaar.bibtex.database.DBUtil;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 16.01.17.
 */
public class TechreportDaoImpl {
	  //Delete techreportObject in Datasource
    public TechreportEntity delete(TechreportEntity techreport) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(techreport);
        s.getTransaction().commit();
        s.close();
        return techreport;
    }

    public TechreportEntity update(TechreportEntity techreport) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(techreport);
        s.getTransaction().commit();
        s.close();
        return techreport;
    }

    public TechreportEntity insert(TechreportEntity techreport) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(techreport);
        s.getTransaction().commit();
        s.close();
        return techreport;
    }

    public List<TechreportEntity> getAlltechreport() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM TechreportEntity a";
            Query query = s.createQuery(queryString);
           List<TechreportEntity> techreportList = query.list();
            s.getTransaction().commit();
            
            return techreportList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public TechreportEntity existstechreport(Techreport a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from TechreportEntity where author = :author AND title = :title AND profid = :profid AND year = :year");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        query.setParameter("institution",a.getInstitution());
        TechreportEntity techreport = (TechreportEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(techreport != null) {
            System.out.println(techreport.getId());
            return techreport;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
