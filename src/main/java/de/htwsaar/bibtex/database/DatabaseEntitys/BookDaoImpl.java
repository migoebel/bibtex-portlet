package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.ObjectClasses.BookEntity;
import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.InterfaceObjects.Book;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by mikegoebel on 19.12.16.
 */
public class BookDaoImpl {

    //Delete bookObject in Datasource
    public BookEntity delete(BookEntity book) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(book);
        s.getTransaction().commit();
        s.close();
        return book;
    }

    public BookEntity update(BookEntity book) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(book);
        s.getTransaction().commit();
        s.close();
        return book;
    }

    public BookEntity insert(BookEntity book) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(book);
        s.getTransaction().commit();
        s.close();
        return book;
    }

    public List<BookEntity> getAllbook() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM BookEntity a";
            Query query = s.createQuery(queryString);
           List<BookEntity> bookList = query.list();
            s.getTransaction().commit();
            
            return bookList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public BookEntity existsbook(Book a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from BookEntity where author = :author AND title = :title AND profid = :profid AND year = :year AND publisher = :publisher");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        query.setParameter("publisher", a.getPublisher());
        BookEntity book = (BookEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(book != null) {
            System.out.println(book.getId());
            return book;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
