package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.ObjectClasses.UnpublishedEntity;
import de.htwsaar.bibtex.InterfaceObjects.Unpublished;
import de.htwsaar.bibtex.database.DBUtil;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 16.01.17.
 */
public class UnpublishedDaoImpl {
	  //Delete unpublishedObject in Datasource
    public UnpublishedEntity delete(UnpublishedEntity unpublished) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(unpublished);
        s.getTransaction().commit();
        s.close();
        return unpublished;
    }

    public UnpublishedEntity update(UnpublishedEntity unpublished) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(unpublished);
        s.getTransaction().commit();
        s.close();
        return unpublished;
    }

    public UnpublishedEntity insert(UnpublishedEntity unpublished) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(unpublished);
        s.getTransaction().commit();
        s.close();
        return unpublished;
    }

    public List<UnpublishedEntity> getAllunpublished() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM UnpublishedEntity a";
            Query query = s.createQuery(queryString);
           List<UnpublishedEntity> unpublishedList = query.list();
            s.getTransaction().commit();
            
            return unpublishedList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public UnpublishedEntity existsunpublished(Unpublished a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from UnpublishedEntity where author = :author AND title = :title AND profid = :profid AND note = :note");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("note",a.getNote());
        UnpublishedEntity unpublished = (UnpublishedEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(unpublished != null) {
            System.out.println(unpublished.getId());
            return unpublished;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
