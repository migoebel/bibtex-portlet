package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.ObjectClasses.InproceedingsEntity;
import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.InterfaceObjects.Inproceedings;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 11.01.17.
 */
public class InproceedingsDaoImpl {
	  //Delete inproceedingsObject in Datasource
    public InproceedingsEntity delete(InproceedingsEntity inproceedings) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(inproceedings);
        s.getTransaction().commit();
        s.close();
        return inproceedings;
    }

    public InproceedingsEntity update(InproceedingsEntity inproceedings) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(inproceedings);
        s.getTransaction().commit();
        s.close();
        return inproceedings;
    }

    public InproceedingsEntity insert(InproceedingsEntity inproceedings) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(inproceedings);
        s.getTransaction().commit();
        s.close();
        return inproceedings;
    }

    public List<InproceedingsEntity> getAllinproceedings() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM InproceedingsEntity a";
            Query query = s.createQuery(queryString);
           List<InproceedingsEntity> inproceedingsList = query.list();
            s.getTransaction().commit();
            
            return inproceedingsList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public InproceedingsEntity existsinproceedings(Inproceedings a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from InproceedingsEntity where author = :author AND title = :title AND profid = :profid AND year = :year AND booktitle = :booktitle");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        query.setParameter("booktitle",a.getBooktitle());
        InproceedingsEntity inproceedings = (InproceedingsEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(inproceedings != null) {
            System.out.println(inproceedings.getId());
            return inproceedings;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
