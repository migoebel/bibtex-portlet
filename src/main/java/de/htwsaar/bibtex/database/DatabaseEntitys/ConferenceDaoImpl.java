package de.htwsaar.bibtex.database.DatabaseEntitys;


import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.InterfaceObjects.Conference;
import de.htwsaar.bibtex.ObjectClasses.ConferenceEntity;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

/**
 * Created by mikegoebel on 11.01.17.
 */
public class ConferenceDaoImpl {
	  //Delete conferenceObject in Datasource
    public ConferenceEntity delete(ConferenceEntity conference) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(conference);
        s.getTransaction().commit();
        s.close();
        return conference;
    }

    public ConferenceEntity update(ConferenceEntity conference) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(conference);
        s.getTransaction().commit();
        s.close();
        return conference;
    }

    public ConferenceEntity insert(ConferenceEntity conference) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(conference);
        s.getTransaction().commit();
        s.close();
        return conference;
    }

    public List<ConferenceEntity> getAllconference() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM ConferenceEntity a";
            Query query = s.createQuery(queryString);
           List<ConferenceEntity> conferenceList = query.list();
            s.getTransaction().commit();
            
            return conferenceList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public ConferenceEntity existsconference(Conference a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from ConferenceEntity where author = :author AND title = :title AND profid = :profid AND year = :year AND booktitle = :booktitle");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        query.setParameter("booktitle", a.getBooktitle());
        ConferenceEntity conference = (ConferenceEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(conference != null) {
            System.out.println(conference.getId());
            return conference;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
