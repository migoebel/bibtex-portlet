package de.htwsaar.bibtex.database.DatabaseEntitys;

import de.htwsaar.bibtex.ObjectClasses.MastersthesisEntity;
import de.htwsaar.bibtex.database.DBUtil;
import de.htwsaar.bibtex.InterfaceObjects.Masterthesis;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
/**
 * Created by mikegoebel on 16.01.17.
 */
public class MastersthesisDaoImpl {
	  //Delete masterthesisObject in Datasource
    public MastersthesisEntity delete(MastersthesisEntity masterthesis) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().delete(masterthesis);
        s.getTransaction().commit();
        s.close();
        return masterthesis;
    }

    public MastersthesisEntity update(MastersthesisEntity masterthesis) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(masterthesis);
        s.getTransaction().commit();
        s.close();
        return masterthesis;
    }

    public MastersthesisEntity insert(MastersthesisEntity masterthesis) {
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        s.getSessionFactory().getCurrentSession().saveOrUpdate(masterthesis);
        s.getTransaction().commit();
        s.close();
        return masterthesis;
    }

    public List<MastersthesisEntity> getAllmasterthesis() {
    	Session s =  DBUtil.get().getSession();
        try {
            s.beginTransaction();

            String queryString = "SELECT a FROM MastersthesisEntity a";
            Query query = s.createQuery(queryString);
           List<MastersthesisEntity> masterthesisList = query.list();
            s.getTransaction().commit();
            
            return masterthesisList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;

    }
    public MastersthesisEntity existsmasterthesis(Masterthesis a){
    	Session s =  DBUtil.get().getSession();
        s.beginTransaction();
        Query query = s.createQuery("from MastersthesisEntity where author = :author AND title = :title AND profid = :profid AND year = :year AND school = :school");
        query.setParameter("author",a.getAuthor());
        query.setParameter("title",a.getTitle());
        query.setParameter("profid",a.getProfid());
        query.setParameter("year",a.getYear());
        query.setParameter("school",a.getSchool());
        MastersthesisEntity masterthesis = (MastersthesisEntity) query.uniqueResult();

        s.getTransaction().commit();
        if(masterthesis != null) {
            System.out.println(masterthesis.getId());
            return masterthesis;
        }else{
            System.out.println("Es wurde kein Eintrag für Befehl: ");
            return null;
        }
    }
}
