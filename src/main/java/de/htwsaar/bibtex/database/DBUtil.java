package de.htwsaar.bibtex.database;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import de.htwsaar.bibtex.ObjectClasses.ArticleEntity;
import de.htwsaar.bibtex.ObjectClasses.BookEntity;
import de.htwsaar.bibtex.ObjectClasses.BookletEntity;
import de.htwsaar.bibtex.ObjectClasses.ConferenceEntity;
import de.htwsaar.bibtex.ObjectClasses.InbookEntity;
import de.htwsaar.bibtex.ObjectClasses.IncollectionEntity;
import de.htwsaar.bibtex.ObjectClasses.InproceedingsEntity;
import de.htwsaar.bibtex.ObjectClasses.ManualEntity;
import de.htwsaar.bibtex.ObjectClasses.MastersthesisEntity;
import de.htwsaar.bibtex.ObjectClasses.MiscEntity;
import de.htwsaar.bibtex.ObjectClasses.PhdthesisEntity;
import de.htwsaar.bibtex.ObjectClasses.ProceedingsEntity;
import de.htwsaar.bibtex.ObjectClasses.TechreportEntity;
import de.htwsaar.bibtex.ObjectClasses.UnpublishedEntity;

/**
 * Helper Klasse zum erstellen der SessionFactory.
 * 
 * @author cedosw
 *
 */
public class DBUtil {
    
    /** Singleton */
    private static DBUtil instance;
    
    private SessionFactory sessionFactory;
    
    /** Liefert die einzige Instanz dieser Klasse */
    public static DBUtil get() {
        if (DBUtil.instance == null) {
            DBUtil.instance = new DBUtil();
        }
        
        return DBUtil.instance;
    }
    
    private DBUtil() {
        this.sessionFactory = getConfiguration().buildSessionFactory();
    }
    
    /** Liefert die Konfiguration für die SessionFactory. */
    public Configuration getConfiguration() {
        Configuration c = new Configuration()
                .addPackage("de.htwsaar.bibtex.ObjectClasses")
                .addAnnotatedClass(ArticleEntity.class)
                .addAnnotatedClass(BookEntity.class)
                .addAnnotatedClass(BookletEntity.class)
        		.addAnnotatedClass(ConferenceEntity.class)
        		.addAnnotatedClass(InbookEntity.class)
        		.addAnnotatedClass(IncollectionEntity.class)
        		.addAnnotatedClass(InproceedingsEntity.class)
        		.addAnnotatedClass(ManualEntity.class)
        		.addAnnotatedClass(MastersthesisEntity.class)
        		.addAnnotatedClass(MiscEntity.class)
        		.addAnnotatedClass(PhdthesisEntity.class)
        		.addAnnotatedClass(ProceedingsEntity.class)
        		.addAnnotatedClass(TechreportEntity.class)
        		.addAnnotatedClass(UnpublishedEntity.class);
        
        return c;
    }
    
    public Session getSession() {
        return sessionFactory.openSession();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
