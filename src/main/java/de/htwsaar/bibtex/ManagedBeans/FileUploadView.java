package de.htwsaar.bibtex.ManagedBeans;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.jbibtex.ParseException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import de.htwsaar.bibtex.ServiceRepository.FileUploadService;
import de.htwsaar.bibtex.ServiceRepository.UserService;


@ManagedBean(name = "FileUploadView")
@RequestScoped
public class FileUploadView {
	
	private UploadedFile uploadedFile;
	private FileUploadService filerepository;
	private String filename; 
	private long userId;
	private static final String PATH = "C:\\Users\\mikegoebel\\Desktop\\Liferay";
	
	 public void handleFileUpload(FileUploadEvent event) throws IOException, ParseException {
		 	save();
	        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " ist hochgeladen.");
	        FacesContext.getCurrentInstance().addMessage(null, message);
	        filerepository.importBibTex(PATH+"/"+filename,this.userId);
	    }
	 
	 private void save() throws IOException{
		 String filename = FilenameUtils.getName(uploadedFile.getFileName());
	     InputStream input = uploadedFile.getInputstream();
	     OutputStream output = new FileOutputStream(new File(PATH, filename));
	    

	     try {
	    	 IOUtils.copy(input, output);
	    	 setFilename(filename);
	     } finally {
	    	 IOUtils.closeQuietly(input);
	    	 IOUtils.closeQuietly(output);
	     }
	 }
	 
	 @PostConstruct
	    public void init() {
	       this.userId = UserService.getCurrentUserId();
	    }
	 
	 public UploadedFile getFile() {
		 return uploadedFile;
	 }

	 public void setFile(UploadedFile uploadedFile) {
		 this.uploadedFile = uploadedFile;

	 }

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public static String getPath() {
		return PATH;
	}
	 
}