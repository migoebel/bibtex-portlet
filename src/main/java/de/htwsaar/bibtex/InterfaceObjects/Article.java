package de.htwsaar.bibtex.InterfaceObjects;

/**
 * Created by mikegoebel on 25.01.17.
 */
public interface Article {

    public int getId();

    public void setId(int id);

    public String getAuthor();

    public void setAuthor(String author);

    public String getTitle();

    public void setTitle(String title);

    public String getJournal();

    public void setJournal(String journal);

    public String getYear();

    public void setYear(String year);

    public String getVolume();

    public void setVolume(String volume);

    public String getNumber();

    public void setNumber(String number);

    public String getPages();

    public void setPages(String pages);

    public String getMonth();

    public void setMonth(String month);

    public String getNote();

    public void setNote(String note);
    long getProfid();
    void setProfid(long profid);
}
