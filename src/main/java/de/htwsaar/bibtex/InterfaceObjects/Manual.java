package de.htwsaar.bibtex.InterfaceObjects;

/**
 * Created by mikegoebel on 25.01.17.
 */
public interface Manual {

    public int getId();

    public void setId(int id);

    public String getAddress();

    public void setAddress(String address);

    public String getTitle();

    public void setTitle(String title);

    public String getYear();

    public void setYear(String year);

    public String getAuthor();

    public void setAuthor(String author);

    public String getOrganization();

    public void setOrganization(String organization);

    public String getEdition();

    public void setEdition(String edition);

    public String getMonth();

    public void setMonth(String month);

    public String getNote();

    public void setNote(String note);
    long getProfid();
    void setProfid(long profid);
}
