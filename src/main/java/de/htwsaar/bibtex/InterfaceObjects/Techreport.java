package de.htwsaar.bibtex.InterfaceObjects;

/**
 * Created by mikegoebel on 25.01.17.
 */
public interface Techreport {

    public int getId();

    public void setId(int id);

    public String getAuthor();

    public void setAuthor(String author);

    public String getTitle();

    public void setTitle(String title);

    public String getInstitution();

    public void setInstitution(String institution);

    public String getYear();

    public void setYear(String year);

    public String getType();

    public void setType(String type);

    public String getNote();

    public void setNote(String note);

    public String getNumber();

    public void setNumber(String number);

    public String getAddress();

    public void setAddress(String address);

    public String getMonth();

    public void setMonth(String month);
    long getProfid();
    void setProfid(long profid);
}
