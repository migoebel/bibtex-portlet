package de.htwsaar.bibtex.InterfaceObjects;

/**
 * Created by mikegoebel on 25.01.17.
 */
public interface Masterthesis {

    public int getId();

    public void setId(int id);

    public String getAuthor();

    public void setAuthor(String author);

    public String getTitle();

    public void setTitle(String title);

    public String getSchool();

    public void setSchool(String school);

    public String getYear();

    public void setYear(String year);

    public String getType();

    public void setType(String type);

    public String getAddress();

    public void setAddress(String address);

    public String getMonth();

    public void setMonth(String month);

    public String getNote();

    public void setNote(String note);
    long getProfid();
    void setProfid(long profid);
}
