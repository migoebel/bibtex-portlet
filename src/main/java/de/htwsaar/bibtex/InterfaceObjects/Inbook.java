package de.htwsaar.bibtex.InterfaceObjects;

/**
 * Created by mikegoebel on 25.01.17.
 */
public interface Inbook {

    public int getId();

    public void setId(int id);

    public String getAuthor();

    public void setAuthor(String author);

    public String getTitle();

    public void setTitle(String title);

    public String getChapter();

    public void setChapter(String chapter);

    public String getPages();

    public void setPages(String pages);

    public String getPublisher();

    public void setPublisher(String publisher);

    public String getYear();

    public void setYear(String year);

    public String getVolume();

    public void setVolume(String volume);

    public String getSeries();

    public void setSeries(String series);

    public String getType();

    public void setType(String type);

    public String getAddress();

    public void setAddress(String address);

    public String getEdition();

    public void setEdition(String edition);

    public String getMonth();

    public void setMonth(String month);

    public String getNote();

    public void setNote(String note);
    long getProfid();
    void setProfid(long profid);
}
