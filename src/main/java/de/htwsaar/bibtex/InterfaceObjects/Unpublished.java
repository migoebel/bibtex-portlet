package de.htwsaar.bibtex.InterfaceObjects;

/**
 * Created by mikegoebel on 25.01.17.
 */
public interface Unpublished {

    public int getId();

    public void setId(int id);

    public String getAuthor();

    public void setAuthor(String author);

    public String getTitle();

    public void setTitle(String title);

    public String getNote();

    public void setNote(String note);

    public String getMonth();

    public void setMonth(String month);

    public String getYear();

    public void setYear(String year);
    long getProfid();
    void setProfid(long profid);
}
