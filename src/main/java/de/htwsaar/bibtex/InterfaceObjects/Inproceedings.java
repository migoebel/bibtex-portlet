package de.htwsaar.bibtex.InterfaceObjects;

/**
 * Created by mikegoebel on 25.01.17.
 */
public interface Inproceedings {

    public int getId();

    public void setId(int id);

    public String getAuthor();

    public void setAuthor(String author);

    public String getTitle();

    public void setTitle(String title);

    public String getBooktitle();

    public void setBooktitle(String booktitle);

    public String getYear();

    public void setYear(String year);

    public String getEditor();

    public void setEditor(String editor);

    public String getVolume();

    public void setVolume(String volume);

    public String getSeries();

    public void setSeries(String series);

    public String getPages();

    public void setPages(String pages);

    public String getAddress();

    public void setAddress(String address);

    public String getMonth();

    public void setMonth(String month);

    public String getOrganization();

    public void setOrganization(String organization);

    public String getPublisher();

    public void setPublisher(String publisher);

    public String getNote();

    public void setNote(String note);
    long getProfid();
    void setProfid(long profid);
}
